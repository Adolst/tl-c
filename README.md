# TL-C protocol in Murphi

## Introduction
TL-C(Tilelink Cached) is a standard interface for on-chip communication in modern computer systems. It provides a scalable, low-latency and coherent way of exchanging data between different components of a System-on-Chip (SoC) design, such as processors, accelerators, and memory controllers. They enable the creation of homogeneous and heterogeneous many-core systems by providing a simple and uniform way of accessing memory and I/O resources. In this work, we model hierarchy cache coherence protocols based on TL-C standard in Murphi, and use model check tool to verify it.

## Files
### generate.py
The program **generate.py** can generate the protocol automatically according to the parameters entered by user. There are 4 parameters that determine the structure of coherence tree, inclusion policies and data control: 
- d(depth): Determine the height of the tree(default: 2);
- b(branch): Determine the number of children for each node(default: 2);
- i(inclusion): Decide whether to adopt the inclusive or non-inclusive policy(default: inclusive);
- c(data control): Decide whether to add data and related controls(default: False).

For example, you can run **generate.py** with
```
python generate.py -d 3 -b 2 -i inclusive -c true
```
to obtain a protocol with 3-Levels cache coherence tree, it adopts the inclusive policy to maintain data consistency, as shown below:

![](/fig/Hierarchy.png)

### TL_states
The directory **TL_states** contains a program that can generate all possible coherence trees by runing main.py.
you can run **main.py** to get all the coherence trees.

### RA_lab
**RA_lab** holds the results of reachability analysis. It is divided into three subdirectories： **CoherenceTrees**,**Invariants** and **Traces**. The **CoherenceTrees** saves the visual coherence trees of TL-C cache hierarchy; **Invariants** holds the results of transforming the state of coherence trees in **CoherenceTrees** into invariants; The subdirectory **Traces** details all the traces from an initial state to a goal state. Next we illustrate these three subdirectories with an example, which is a coherence tree of Non-Inclusive cache hierarchy with 3 levels.

![](/fig/RA_lab_tree.png)
 
#### CoherenceTrees
The figure above, representing a reachable coherence tree, is the 2nd case of [Non-Inclusive_Tree_3Levels.txt](./RA_lab/CoherenceTrees/Non-Inclusive/Non-Inclusive_Tree_3Levels.txt):

```
2
TLState.INVALID
├── TLState.INVALID
│   ├── TLState.INVALID
│   └── TLState.INVALID
└── TLState.INVALID
    ├── TLState.INVALID
    └── TLState.BRANCH
```
#### Invariants
To analyse the reachability of it in the Murphi, we negate the logical conjunction of the cache states of all tree nodes, taking the result as the corresponding invariant, as shown below. This example is the 2nd case of [Non-Inclusive_Invariants_3Levels.txt](./RA_lab/Invariants/Non-Inclusive/Non-Inclusive_Invariants_3Levels.txt).

```
invariant "test_reachable_state_2"
    !( Tree[1].cache.state = None & Tree[2].cache.state = None & Tree[4].cache.state = None & Tree[5].cache.state = None & Tree[3].cache.state = None & Tree[6].cache.state = None & Tree[7].cache.state = Branch );
```
In the next step it's put in the [TL-C_NonIn.m](./Non-Inclusive/TL-C_NonIn.m) or [TL-C_NonIn_data.m](./Non-Inclusive/TL-C_NonIn_data.m) and checked by Murphi. When this invariant is violated, it indicates that the corresponding cache coherence tree is reachable for the current protocol model. All invariants of NonInclusive TL-C, including this one, are stored in the file [Non-Inclusive_Invariants_2Levels.txt](./RA_lab/Invariants/Non-Inclusive/Non-Inclusive_Invariants_2Levels.txt) and [Non-Inclusive_Invariants_3Levels.txt](./RA_lab/Invariants/Non-Inclusive/Non-Inclusive_Invariants_3Levels.txt)(the *n*Levels means a coherence tree with *n* Levels). Notice that the structure of coherence tree has nothing to do with data, so TL-C_NonIn and TL-C_NonIn_Data have the same invariants.

#### Traces
To help the reader better understand how these states are reached, we output the traces from the initial states to the goal states with the local search algorithm, and has saved them in the four directories: **RA_lab/Traces/TL-C_In**, **RA_lab/Traces/TL-C_In_data**, **RA_lab/Traces/TL-C_NonIn**, **RA_lab/Traces/TL-C_NonIn_data**, which correspond to four protocols. For example, protocol TL-C_NonIn starts from the initial state and takes 14 steps to reach the "test_reachable_state_2", which is recorded in the file [state_2.log](./RA_lab/Traces/TL-C_NonIn/3Levels/state_2.log).

All results of reachability analysis are shown below, clicking on the link will jump to the statistical table for the corresponding model. 

|       Protocol       | #Levels | #Coherence Trees |     RA result file link    |
|:--------------------:|:------:|:----------------:|:--------------------------:|
|        TL-C-In       |    2   |         7        | [statistical_table1](./RA_lab/Traces/TL-C_In/2Levels/RA_result_sheet1.md) |
|                      |    3   |         38       | [statistical_table2](./RA_lab/Traces/TL-C_In/3Levels/RA_result_sheet2.md) |
|     TL-C-In\_data    |    2   |         7        | [statistical_table3](./RA_lab/Traces/TL-C_In_data/2Levels/RA_result_sheet3.md) |
|                      |    3   |         38       | [statistical_table4](./RA_lab/Traces/TL-C_In_data/3Levels/RA_result_sheet4.md) |
|    NI-TL\_C-NonIn    |    2   |         16       | [statistical_table5](./RA_lab/Traces/TL-C_NonIn/2Levels/RA_result_sheet5.md) |
|                      |    3   |         224      | [statistical_table6](./RA_lab/Traces/TL-C_NonIn/3Levels/RA_result_sheet6.md) |
| NI-TL\_C-NonIn\_data |    2   |         16       | [statistical_table7](./RA_lab/Traces/TL-C_NonIn_data/2Levels/RA_result_sheet7.md) |
|                      |    3   |         224      | [statistical_table8](./RA_lab/Traces/TL-C_NonIn_data/3Levels/RA_result_sheet8.md) |


For example, the statistical table for Inclusive cache hierarchy with 3 levels and data(statistical_table4) is as follows:

| state_no | IsReachable | Time(sec) |      Trace file link         |
|----------|-------------|-----------|------------------------------|
| 1        | True        | 1.30s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_1.log)  |
| 2        | True        | 1.88s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_2.log)  |
| 3        | True        | 1.69s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_3.log)  |
| 4        | True        | 1.76s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_4.log)  |
| 5        | True        | 1.90s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_5.log)  |
| 6        | True        | 2.06s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_6.log)  |
| 7        | True        | 5.87s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_7.log)  |
| 8        | True        | 1.83s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_8.log)  |
| 9        | True        | 1.82s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_9.log)  |
| 10       | True        | 1.73s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_10.log) |
| 11       | True        | 1.81s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_11.log) |
| 12       | True        | 1.90s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_12.log) |
| 13       | True        | 5.53s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_13.log) |
| 14       | True        | 1.73s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_14.log) |
| 15       | True        | 1.75s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_15.log) |
| 16       | True        | 1.77s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_16.log) |
| 17       | True        | 1.82s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_17.log) |
| 18       | True        | 2.04s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_18.log) |
| 19       | True        | 1.84s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_19.log) |
| 20       | True        | 1.70s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_20.log) |
| 21       | True        | 1.72s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_21.log) |
| 22       | True        | 2.12s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_22.log) |
| 23       | True        | 2.29s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_23.log) |
| 24       | True        | 2.52s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_24.log) |
| 25       | True        | 2.42s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_25.log) |
| 26       | True        | 4.96s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_26.log) |
| 27       | True        | 2.37s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_27.log) |
| 28       | True        | 3.42s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_28.log) |
| 29       | True        | 3.51s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_29.log) |
| 30       | True        | 8.75s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_30.log) |
| 31       | True        | 1.83s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_31.log) |
| 32       | True        | 2.49s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_32.log) |
| 33       | True        | 2.53s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_33.log) |
| 34       | True        | 6.51s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_34.log) |
| 35       | True        | 3.93s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_35.log) |
| 36       | True        | 6.55s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_36.log) |
| 37       | True        | 6.53s     | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_37.log) |
| 38       | True        | 18.73s    | [trace_file](./RA_lab/Traces/TL-C_In_data/3Levels/state_38.log) |

In this table, state_no represents the identification of each cache consistency tree in the corresponding version reachability analysis experiment; IsReachable will be True if the search algorithm can reach this tree; Time means the time it takes for the search algorithm to reach this tree; Trace file link provides a link to the corresponding trace file in this code repository. 

### Inclusive and Non-Inclusive
The directories **Inclusive** and **Non-Inclusive** hold the Inclusive and Non-Inclusive protocol model respectively. The protocol files with suffix *data* means they modeled and verified the data control, while the protocol files without suffix *data* just verified the transactions and permission transitions. 

### inclusive.part and non-inclusive.part
The **inclusive.part** classifies all possible occurrences of an inclusive coherence tree as:
1. There is a core has data block and read+write permission(Tip) 
2. At lease one core has data block and read permission(Branch) 
3. Any core is Invalid(without data block)

The **non-inclusive.part** is similar to **inclusive.part** except that the classification object is a non-inclusive coherence tree.

## TL-C
### States
The fundamental permissions it is possible for a particular agent’s copy of a block to have are None, Read, or Read+Write. The permissions available on
a particular cached copy depend on the current state of copies in the cache hierarchy, as described below.  
- None. A node has no cache a copy of the data.
- Branch. A node has Read-only permission on its copy.
- Trunk. A node with a cached copy has neither Read nor Write permissions on it.
- Tip. A node has both the Read and Write permission on its cached copy.  

At any given time, some subset of those nodes actually contain copies of cached data. These nodes form a Coherence Tree, which has been shown in
the following figure. The TileLink coherence protocol policies require the tree to grow and shrink in response to memory access operations. Inclusive policies require that the parent cache includes the lines in child caches. In Murphi, we model TileLink state by a type:
```
stateType: enum{None,Trunk,Branch,Tip};
```
![The possible states in TileLink protocol](./fig/states.png)

### Transfer Operations, Messages, Channels, and Permissions
There are transfer operations which transfer a copy of a block of data to a new location in the memory hierarchy, as shown below:
- Acquire: Creates a new copy of a block (or particular permissions on it) in the requesting master. AcquireBlock is to obtain both the data and permission, while the AcquirePerm is to upgrade the permission based on the existing block.
- Release: Relinquishes a copy of the block (or particular permissions on it) back to the slave from the requesting master. ReleaseData or Release means a copy of the block (or particular permissions on it) back to the slave node from the master node, and needs the ReleaseAck as respond.
- Probe: Forcibly removes of a copy of the block (or particular permissions on it) from a master to the requesting slave. When the slave node is not sure whether the master node has a dirty block, ProbeBlock is sent, otherwise ProbePerm is sent. If a master node do has a dirty block, it responds with ProbeAckData, or else sends ProbeAck

Acquire operations grow the tree, either by extending the trunk or by adding a new branch from an existing branch or the tip. In order to do so, the old trunk or branches may have to be pruned with recursive Probe operations before the new branch can be grown. Release operations prune the tree by voluntarily shrinking it, typically in response to cache capacity conflicts.

To support transfer operations, Five channels are used to communicate messages among nodes. These messages are illustrated in the following table. The $\mathtt{Message}$ represents the message name to be sent, and $\mathtt{Operation}$ indicates that it is divided into a class of operations. $\mathtt{Channel}$ is the channel code for the message transmission. The $\mathtt{Response}$ column means that when a message is sent, what kind of respond dose it expect.

|    Message   | Operation | Channel |  Response  |
|:------------:|:--------:|:-------:|:----------:|
|  AcuireBlock |  Acquire |    A    |    Grant   |
|  AcuirePerm  |  Acquire |    A    |    Grant   |
|  ProbeBlock  |   Probe  |    B    |  ProbeAck  |
|   ProbePerm  |   Probe  |    B    |  ProbeAck  |
| ProbeAckData |   Probe  |    C    |      -     |
|   ProbeAck   |   Probe  |    C    |      -     |
|  ReleaseData |  Release |    C    | ReleaseAck |
|    Release   |  Release |    C    | ReleaseAck |
|  ReleaseAck  |  Release |    D    | ReleaseAck |
|     Grant    |  Acquire |    D    |  GrantAck  |
|   GrantData  |  Acquire |    D    |  GrantAck  |
|   GrantAck   |  Acquire |    E    |      -     |

We define the message type in Murphi as:
```
AType: enum{AcquirePerm, AcquireBlock};
BType: enum{ProbePerm, ProbeBlock};
CType: enum{ProbeAck, ProbeAckData, Release, ReleaseData};
DType: enum{Grant, GrantData, ReleaseAck};
EType: enum{GrantAck}
```
Transfers logically operate on permissions, and so messages that comprise them must specify an intended outcome: an upgrade to more permissions, a downgrade to fewer permissions, or a no-op leaving permissions unchanged.
```
growType:  enum{NtoB, NtoT, BtoT};
pruneType: enum{BtoN, TtoN, TtoB, NtoN};
capType:   enum{toN, toB, toT};
```
As we mentioned previously, five channels are used to support the transfer permission operations, which can be marked as A, B, C, D, E, respectively, as shown in the following figure.
![The link in TileLink network](./fig/link.png)
To be specific, a master node can acquire permission to read or write a copy of a cache block through channel A, a slave node can queries or modifies a master’s permissions on a cached data block through channel B, a master node can acknowledges a message in channel B through channel C, or release data block voluntarily. Through channel D, the slave node can provide data or permissions to the master node, which receives acknowledgment from master in channel E. In Murphi, Channels are defined primarily by the types of messages that each channel can send and the parameters in those messages:
```
ChAtype: array[branchType] of record
    opcode: AType;
    para: growType;
end;

ChBType: array[branchType] of record
    opcode: BType;
    para: capType;
end;

ChCType: array[branchType] of record
    opcode: CType;
    para: pruneType;
end;

ChDType: array[branchType] of record
    opcode: DType;
    para: capType;
end;

ChEType: array[branchType] of record
    opcode: EType;
end;
```
### Pending flag
The request messages generate response messages, and response messages are guaranteed to eventually make forward progress. However, under certain conditions, recursive request messages targeting the same block leads to incorrect
states and error in data. In order to avoid such a situation, concurrency limits must be placed on request and response messages.
- Acquire: A master should not issue an Acquire if there is a pending Grant on the block. Once the Acquire is issued the master should not issue further Acquires on that block until it receives a Grant.
- Grant: A slave should not issue a Grant if there is a pending ProbeAck on the block. Once the Grant is issued, the slave should not issue Probeson that block until it receives a GrantAck.
- Release: master should not issue a Release if there is a pending Grant on the block. Once the Release is issued, the master should not issue ProbeAcks, Acquires, or further Releases until it receives a ReleaseAck from the slave acknowledging completion of the writeback.
- Probe: A slave should not issue a Probe if there is a pending GrantAck on the block. Once the Probe is issued, the slave should not issue further Probes on that block until it receives a ProbeAck.
As shown in the table 1, the request message sent from the master interface can only be Acquire, AcquireBlock, Release, ReleaseData, and the request message sent from the slave interface can only be ProbePerm, ProbeBlock, Grant, GrantData. Therefore, we define types named “masterPendingType” and “slavePendingType” in Murphi according to the response messages:
```
slavePendingType: enum{pending ProbeAck, pending ProbeAckData, pending GrantAck};
masterPendingType: enum{pending Grant, pending GrantData, pending ReleaseAck};
```
### A Node in TileLink
After all about a node are introduced, now we can define a node in Murphi:  
In this work, the variable of array type is used to hold the tree structure. The size of the array is the same as the number of nodes, which is set as 7. Each node record the index of children to keep the tree structure, which is named sons. The index of nodes in the array is consistent with the encoding rules of full binary tree. In the other word, when a node is numbered i, its left child is numbered 2\*i, and its right child is numbered 2*i+1.
There are other variables to guarantees the cache coherence property. For example, it is necessary to preserve permission of the cache block by using TileLink states in a variable “cache”, and “directory” is used to hold the states of its children. The sent requests needs responds, so “master pending” represents the reply that a request sent from the master interface is waiting for, and the “slave pending” represents a request sent from the slave interface is waiting for. The status of channel and the messages in it are presented by “chan1” to “chan5”.
```
nodeId:     1..7;
l1Id:     4..7;
cacheId:    1..3;
branchType: 1..2;
TreeNode: Record
    cache: record state: stateType;
    sons: array[branchType] of nodeId;
    directory: array[branchType] of stateType;
    slave pending: array[branchType] of slave pending Type;
    master pending: master pending Type;
    chanA: ChAType;
    chanB: ChBType;
    chanC: ChCType;
    chanD: ChDType;
    chanE: ChEType;
end;

Var
Tree: array [nodeId] of treeNode;
```
![The representation of a tree structure in an array variable](./fig/array_var.png)

## TileLink transactions and their transitions
There are  three kinds of transactions: Acquire, Probe, Release. An Acquire transaction involves   transitions that create new cache block or upgrade permission, along with the corresponding response messages; a Probe transaction forcibly removes of a copy of the block (or particular permissions on it) from a master to the requesting slave; a Release transaction relinquishes a copy of the block (or particular permissions on it) back to the slave from the requesting master.

### Inclusive Transactions
#### Acquire Transaction
1. At first, an L1 node in the None state sends a request to obtain a new copy of a block and Branch permission on it, which is represented by AcquireBlock(NtoB). In this rule, L1-cache node must check its master_pending. If there has been a pending Grant or ReleaseAck on the block, it shouldn't issue such a message, otherwise send a AcquireBlock(NtoB) to the link(chanA) connected to L2 node and set master_pending to be pending_GrantData. $\mathtt{IsUndefined}$ is a built-in function that can be used to determine whether a variable has the undefined value. Here  we use  to determine whether there is a message in the channel or whether the pending flag is empty in a way that avoids the need for additional type and thus reduces the reachable space.
```
ruleset i: l1Id; j: cacheId; b: branchType do
rule "send_AcquireBlock_toB"
    Tree[j].sons[b] = i &
    Tree[i].cache.state = None &
    IsUndefined(Tree[i].master_pending)
==>
begin
    Tree[j].chanA[b].opcode  := AcquireBlock;
    Tree[j].chanA[b].para    := NtoB;
    Tree[i].master_pending := pending_GrantData;
endrule;
endruleset;
```
2. This message is forwarded to the L2 node. In the following rule, when L2 node checks and finds a message AcquireBlock(NtoB) coming from the chanA, it needs to respond GrantData(toB). However, it is in the None state and doesn't cache the copy of the block. Therefore, this request becomes the request of the L2 node, to be sent to its parent L3 node. In fact, this rule is designed to apply to all nodes except l1 and the last layer cache, so that such rules do not need to be modified even if the structure of the tree changes.
```
ruleset i: cacheId; j: cacheId; b1: branchType; b2: branchType do
rule "Ln_receive_AcquireBlock_NtoB_None"
    Tree[j].sons[b2] = i &
    !IsUndefined(Tree[i].chanA[b1].opcode) &
    Tree[i].chanA[b1].opcode = AcquireBlock &
    Tree[i].chanA[b1].para = NtoB &
    Tree[i].cache.state = None &
    IsUndefined(Tree[i].master_pending)
==>
begin
    Tree[j].chanA[b2].opcode := AcquireBlock;
    Tree[j].chanA[b2].para := NtoB;
    Tree[i].master_pending := pending_GrantData;
endrule;
endruleset;
```
3. Next, L3 node receives the request from L2 node. Since its own state is also None, it first needs to obtain the cache block. In this protocol, the communication between L3 node and memory has no extra spending. In other words, L3 node can directly change its state without sending messages to memory.
```
ruleset b: branchType do
rule "L3_receive_AcquireBlock"
    !IsUndefined(Tree[1].chanA[b].opcode) &
    Tree[1].chanA[b].opcode = AcquireBlock &
    Tree[1].cache.state = None
==>
begin
    Tree[1].cache.data := memData;
    Tree[1].cache.state := Tip;
    auxData := memData;
endrule;
endruleset;
```
4. After having a Tip permission, the L3 node is able to responds to the AcquireBlock(NtoB) from L2 node. In advance of sending a GrantData, it is necessary to verify whether a Grant(GrantData) or ProbePerm(ProbeBlock) has already been dispatched to the child and not yet received reply. It then sets the chanA to an undefined value (in this case, null), and sends GrantData(toB) from the chanD, and mark the slave_pending, which means wait for the GrantAck. At the same time, update the state of its child to Branch in the directory. 
```
ruleset i: cahcheId; b: branchType do
rule "Ln_receive_AcquireBlock_NtoB_Tip/Branch"
    !IsUndefined(Tree[i].chanA[b].opcode) &
    Tree[i].chanA[b].opcode = AcquireBlock &
    Tree[i].chanA[b].para = NtoB &
    (Tree[i].cache.state = Branch | Tree[i].cache.state = Tip) &
    IsUndefined(Tree[i].slave_pending[b])
==>
begin
    undefine Tree[i].chanA[b];
    Tree[i].chanD[b].opcode := GrantData;
    Tree[i].chanD[b].para := toB;
    Tree[i].chanD[b].data := Tree[i].cache.data;
    Tree[i].slave_pending[b] := pending_GrantAck;
    Tree[i].directory[b] := Branch;
endrule;
endruleset;
```
5. Upon receipt of GrantData, L2 node can finally proceed to transmit it to L1 node. However, it must transition its state to Branch firstly and reset both the channel and master_pending values to null. In order to complete the Acquire operation between L2 and L3, it also needs to send the GrantAck to L3 node. Here an L1-cache node $i$ is defined as an alias of a son of a L2-cache node $j$, and $i$ is the $b^{st}$ child of $j$. In the following introduction of the other transition rules, we frequently employ the aliasing techniques.
```
ruleset j: cacheId; b: branchType do
alias i: Tree[j].sons[b] do
rule "respond_GrantData_toB"
    !IsUndefined(Tree[j].chanD[b].opcode) &
    Tree[j].chanD[b].opcode = GrantData &
    Tree[j].chanD[b].para = toB
==>
begin
    Tree[i].cache.state := Branch;
    Tree[i].cache.data := Tree[j].chanD[b].data;
    undefine Tree[j].chanD[b];
    undefine Tree[i].master_pending;
    Tree[j].chanE[b].opcode  := GrantAck;
endrule;
endalias;
endruleset;
```
6. Next, L2 node will then repeats step 4 and bypass the GrantData to the L1 node. After receiving the GrantData, L1 node shall repeat step 5 before finally obtaining the cache block and Branch permission. All Acquire operations must be concluded with a GrantAck.
```
ruleset i: cacheId do
rule "Ln_receive_GrantAck"
    !IsUndefined(Tree[i].chanE[b].opcode) &
    Tree[i].chanE[b].opcode = GrantAck
==>
begin
    undefine Tree[i].chanE[b];
    undefine Tree[i].slave_pending[b];
endrule;
```
![acquire transaction](./fig/Acquire_transaction.png)
A scenario that an L1 node send request to obtain a copy of a block and the Branch permission on it. The steps in the figure corresponding to the steps 1 through 6. In the diagram, steps 4, 5, and 6 are repeated. This is due to the fact that they conduct the same operation throughout the process. To save space, we omit some information, such as sending messages into the channels or clearing them.

#### Probe Transaction
1. The L2 has two children(L1-1 and L1-2). At the beginning, the L1-1 node in the None state sends a request to obtain a new copy of a block and Tip permission on it, which is represented by AcquireBlock(NtoT).
```
ruleset i: l1Id; j : cacheId; b : branchType do
rule "send_AcquireBlock_toT"
    Tree[j].sons[b] = i &
    Tree[i].cache.state = None &
    IsUndefined(Tree[i].master_pending)
==>
begin
    Tree[j].chanA[b].opcode  := AcquireBlock;
    Tree[j].chanA[b].para    := NtoT;
    Tree[i].master_pending   := pending_GrantData;
endrule;
endruleset;
```

2. The L2 node discovers that it is in the Trunk state after receiving the AcquireBlock(NtoT) from the L1 node, and another L1 node is in the Tip state. In this case, it has to forcibly remove the copy of the block and particular permissions from the L1-2. 
```
ruleset i: cacheId; b: branchType do
rule "Ln_receive_AcquireBlock_NtoT_Trunk"
    !IsUndefined(Tree[i].chanA[b].opcode) &
    Tree[i].chanA[b].opcode = AcquireBlock &
    Tree[i].chanA[b].para = NtoT &
    Tree[i].cache.state = Trunk &
    exists k : branchType do
        k != b &
        Tree[i].directory[k] = Tip &
        IsUndefined(Tree[i].slave_pending[k])
    end
==>
begin
    for k : branchType do
        if k != b & Tree[i].directory[k] = Tip then       
            Tree[i].chanB[k].opcode := ProbeBlock;
            Tree[i].chanB[k].para := toN;
            Tree[i].slave_pending[k] := pending_ProbeAck;
        endif;
    endfor;
endrule;
endruleset;
```

3. Next, L1-2 receives the request from the L2 node. L1-2 changes its state to None and reply with ProbeAckData(TtoN). 
```
ruleset i: l1Id; j: cacheId; b: branchId do
rule "respond_ProbeBlock_TtoN"
    Tree[j].sons[b] = i &
    !IsUndefined(Tree[j].chanB[b].opcode) &
    Tree[j].chanB[b].opcode = ProbeBlock &
    Tree[j].chanB[b].para = toN &
    (!IsUndefined(Tree[i].master_pending) ->
    Tree[i].master_pending != pending_ReleaseAck)&
    Tree[i].cache.state = Tip
==>
begin
    Tree[j].chanC[b].para    := TtoN; 
    Tree[j].chanC[b].opcode  := ProbeAckData;
    if Tree[i].cache.dirty then
        Tree[j].chanC[b].opcode := ProbeAckData;
        Tree[j].chanC[b].data := Tree[i].cache.data;
        Tree[i].cache.dirty := false;
    endif;
    undefine Tree[i].cache.data;
    undefine Tree[j].chanB[b];
    Tree[i].cache.state := None;
endrule;
endalias;
endruleset;
```
4. Upon receipt of ProbeAckData, L2 node can change its state to Branch firstly and reset both the channel and slave_pending values to null. The directory also need to be updated.
```
ruleset i: cacheId do
rule "Ln_receive_ProbeAckData_TtoN"
    !IsUndefined(Tree[i].chanC[b].opcode) &
    Tree[i].chanC[b].opcode = ProbeAckData &
    Tree[i].chanC[b].para = TtoN
==>
begin
    Tree[i].directory[b] := None;
    Tree[i].cache.state := Tip;
    Tree[i].cache.data := Tree[i].chanC[b].data;
    Tree[i].cache.dirty := true;
    undefine Tree[i].chanC[b];
    undefine Tree[i].slave_pending[b];
endrule;
endruleset;
```
5. Same as the step 4 in the Acquire transaction.
6. Same as the step 5 in the Acquire transaction.
7. Same as the step 6 in the Acquire transaction.

![probe transaction](./fig/Probe_transaction.png)

#### Release Transaction
1. The Release transaction begins with a node actively sending a Release and ends with a ReleaseAck. At first, an L1 node in the Branch state sends a Release(BtoN) to drop a copy of a block. In this rule, L1 node must check its master_pending. If there has been a pending response on the block, it shouldn't issue such a message. Since the ProbeAck and Release share the same channel, we also need to check whether it is occupied before sending.
```
ruleset i: l1Id; j: cacheId; b: branchType do
rule "Ln_send_Release_BtoN"
    Tree[j].sons[b] = i &
    Tree[i].cache.state = Branch &
    IsUndefined(Tree[i].master_pending) &
    IsUndefined(Tree[j].chanC[b].opcode)
==>
begin
    Tree[j].chanC[b].opcode := Release;
    Tree[j].chanC[b].para := BtoN;
    Tree[i].master_pending := pending_ReleaseAck;
    Tree[i].cache.state := None;
endrule;
endalias;
endruleset;
```
2. Next, L2 receives the request and responds with ReleaseAck In addtion, L2 sets the chanC to null and updates its directory. 
```
ruleset i: cacheId; b: branchType do
rule "Ln_receive_Release_BtoN"
    !IsUndefined(Tree[i].chanC[b].opcode) &
    Tree[i].chanC[b].opcode = Release &
    Tree[i].chanC[b].para = BtoN
==>
begin
    undefine Tree[i].chanC[b];
    Tree[i].directory[b] := None;
    Tree[i].chanD[b].opcode := ReleaseAck;
endrule;
endruleset;
```
3. After receiving the Release Ack, the whole Release transaction is finished. Master_pending and chanD are cleared.
```
ruleset j: cacheId; b: branchType do
rule "receive_ReleaseAck"
    !IsUndefined(Tree[j].chanD[b].opcode) &
    Tree[j].chanD[b].opcode = ReleaseAck
==>
begin
    undefine Tree[i].master_pending;
    undefine Tree[j].chanD[b];
endrule;
endalias;
endruleset;
```
![release transaction](./fig/Release_transaction.png)

### Non-Inclusive Transactions
#### Acquire Transaction
In the noninclusive mode, a node with None status can have a non-None status child, which is not permitted in inclusive mode. As a result, when a parent node is in None state in noninclusive mode, it does not indicate that there is no copy of the data in this subtree, as show below. Therefore when the AcquireBlock or AcquirePerm is received, we must verify the status of the other child in the directory. If the child is not none, a Probe should be sent to it. This process can be compared with the step 2 of Inclusive Acquire Transaction.
```
ruleset j: cacheId; b: branchType do
alias i: Tree[j].sons[b] do
rule "Ln_receive_AcquireBlock_NtoB_None_Branch"
    !IsUndefined(Tree[i].chanA[b].opcode) &
    Tree[i].chanA[b].opcode = AcquireBlock &
    Tree[i].chanA[b].para = NtoB &
    Tree[i].cache.state = None &
    exists k : branchType do
        k != b &
        Tree[i].directory[k] = Branch &
        IsUndefined(Tree[i].slave_pending[k])
    end
==>
begin
    for k : branchType do
        if k != b & Tree[i].directory[k] = Branch &
           IsUndefined(Tree[i].slave_pending[k]) then       
            Tree[i].chanB[k].opcode := Probe;
            Tree[i].chanB[k].para := toB;
            Tree[i].slave_pending[k] := pending_ProbeAckData;
        endif;
    endfor;
endrule;
endalias;
endruleset;
```
![](fig/NI_Acquire_transaction.png)

#### Probe Transaction
As the following figure(b) shows, assume that L3 node sends ProbePerm(toN) to an L2 node. After receiving the probe message, even though L2 node is in the None state, it must still pass on the probe because one of its child nodes is in the Branch state. In the inclusive mode, this step is superfluous, as when a node is in None state, its children must also be None, as the figure(a) shows.
```
ruleset i: cacheId; j: cacheId; b: branchType do
rule "Ln_receive_Probe_toN_None_Branch/Tip"
    Tree[j].sons[b] = i &
    !IsUndefined(Tree[j].chanB[b].opcode) &
    Tree[j].chanB[b].opcode = Probe &
    Tree[j].chanB[b].para = toN &
    Tree[i].cache.state = None &
    exists k: branchType do
        (Tree[i].directory[k] = Branch |
         Tree[i].directory[k] = Tip) &
        IsUndefined(Tree[i].slave_pending[k])
    end
==>
begin
    for k: branchType do
        if (Tree[i].directory[k] = Branch |
           Tree[i].directory[k] = Tip) &
           IsUndefined(Tree[i].slave_pending[k]) then
            Tree[i].chanB[k].opcode := Probe;
            Tree[i].chanB[k].para := toN;
            Tree[i].slave_pending[k] := pending_ProbeAckData;
        endif;
    end;
endrule;
```
![](fig/NI_Probe_transaction.png)

#### Release Transaction
If the block has any copy in the children, the parent node can discard it directly. Otherwise the block continues to be released. Specifically, this process can be divided into four cases, which is also illustrated in the figure below:
- If it's a non-leaf node in the Branch state that discards a block, and the node has any child with Branch status, the block should be discarded directly.
- The non-leaf node in the Trunk state discards the block directly.
- If a non-leaf node is in the Tip state, but it has a Branch child, it should informs its parents that there are no more nodes in the subtree that owns Tip permissions. Therefore it sends a Release(TtoB) and discards the block.
- If a node is a leaf or does not meet any of those three conditions, then the block should be released to its parent. Note that since the block is not necessarily in its parent node, it should send ReleaseData with data and dirty information.
![](fig/NI_Release.png)

```
ruleset i : cacheId do
rule "Ln_send_Release_Branch_Branch"
    Tree[i].cache.state = Branch &
    exists k : branchType do
        Tree[i].directory[k] = Branch
    end
==>
begin
    Tree[i].cache.state := None;
endrule;
```


## Invariants analysis
1. Both TL-C-In and TL-C-NonIn should satisfies mutual exclusion and data consistency requirements. The former ensures that only one node has write permission at a time; while the latter assures that the core can access the most fresh copy when it has read or write permission. 
```
invariant "mutual_exclusion"
forall i : coreId; j : coreId do
    i != j ->
    !(Tree[i].cache.state = Tip & Tree[j].cache.state = Tip)
endforall;

invariant "data_coherence"
forall i: nodeId do
    (Tree[i].cache.state = Tip 
    Tree[i].cache.state = Branch) ->
    Tree[i].cache.data = auxData
endforall;
```
2. Both TL-C-In and TL-C-NonIn should meet the specification of TileLink. For example: A node in the Trunk must have a child on which a subtree has Tip permission (and the leaf node cannot be Trunk); When a node has the write permission, no other node can have the read permission.
```
invariant "coherence_tree_1"
forall i : cacheId do
    Tree[i].cache.state = Trunk ->
    exists b :branchType do
        Tree[i].directory[b] = Tip
    end
endforall &
forall i : l1Id do
    Tree[i].cache.state != Trunk 
endforall;

invariant "coherence_tree_2"
forall i : cacheId do
    forall b1 : branchType do
        forall b2 : branchType do
            b1 != b2 & Tree[i].directory[b1] = Tip -> 
            Tree[i].cache.state != Branch & 
            Tree[i].directory[b2] != Branch
        endforall
    endforall
endforall;
```

3. In addition, the inclusive policy requires that data in a node must have a copy in its parent, that is, if a certain parent is None, then all of its children must be None as well
```
invariant "coherence_tree_3"
forall i : cacheId do
    Tree[i].cache.state = None ->
    forall b : branchType do
        Tree[i].directory[b] = None
    endforall
endforall;
```


## Results
### Reachability Analysis
First we use a generator in the **TL_states** to create all possible cache coherence trees in TL-C-In and TL-C-NonIn and their specifications in Murphi.
For a cache coherence tree, we can easily write a specification(as shown in below) to check its reachability. Specifically, we negate the logical conjunction of the cache states of all tree nodes, taking the result as the corresponding invariant. When the invariant is violated, it indicates that the corresponding cache coherence tree is reachable for the current protocol model.
```
invariant "a_typical_reachable_state"
    !(Tree[1].cache.state = Trunk &
        Tree[2].cache.state = Tip & 
        Tree[4].cache.state = Branch &
            Tree[5].cache.state = Branch &
            Tree[3].cache.state = None & 
            Tree[6].cache.state = None & 
            Tree[7].cache.state = None)
```
![](/fig/astate.png)

In this way, we check all the cache coherence trees in the **inclusive.part** and **non-inclusive.part** and conclude that these trees are all reachable. we ran the Murphi code to perform these experiments on a computing server with 64 Intel Xeon processors, 2015~GiB memory and 64-bit Linux 3.10.0. The statistics includes 8 types of data: #Nodes is the number of cache nodes in the system; The #Depth and #Branches together determine the structure and the #Nodes: The full binary tree with two Levelss indicates that it has 3 nodes, whereas the full binary tree with Three Levelss indicates that it has 7 nodes; #Coherence Trees is the number of coherence trees; BFS/DFS/LS(Local Search) indicate that how many coherence trees we actually found with this search algorithm. Due to memory limitations, DFS algorithm can't find all the states. Due to memory limitations, DFS can't find all the states.

|       Protocol       | #Depth | #Branches | #Nodes | #Coherence Trees | BFS | DFS |  LS |
|:--------------------:|:------:|:---------:|:------:|:----------------:|:---:|:---:|:---:|
|        TL-C-In       |    2   |     2     |    3   |         7        |  7  |  7  |  7  |
|                      |    2   |     3     |    7   |         38       |  38 |  38 |  38 |
|     TL-C-In\_data    |    2   |     2     |    3   |         7        |  7  |  7  |  7  |
|                      |    2   |     3     |    7   |         38       |  38 |  38 |  38 |
|    NI-TL\_C-NonIn    |    2   |     2     |    3   |        16        |  16 |  16 |  16 |
|                      |    2   |     3     |    7   |         224      | 224 | 220 | 224 |
| NI-TL\_C-NonIn\_data |    2   |     2     |    3   |        16        |  16 |  16 |  16 |
|                      |    2   |     3     |    7   |         224      | 224 | 217 | 224 |

### Model Checking Results
In the process of modeling the hierarchical protocol, we discovered that there was a special case that had not been taken into account, which resulted in a deadlock state in Murphi, where no rules were available to be executed. 

To be specific, we Intuitively consider that if a node has already cached a block, and needs to write data, it simply sends AcquirePerm(BtoT) for Tip permissions instead of sending the AcquireBlock(NtoT) to request the same block. However, when a node receives AcquirePerm(BtoT) message, is it confirmed that the source node owns a copy of the block? The answer is negative, as the following shows. 

![](fig/deadlock.png)

The L2 has two children(L1-1 and L1-2). At the beginning, L1-1 is in the Branch state and makes a request AcquirePerm(BtoT) for Tip permission, and L1-2 is in the None state and send AcquireBlcok(NtoT) for the data block and Tip permission. However, L2 first receives the AcquireBlock(NtoT) message from L1-2. It checks the directory and finds that the state of L1-1 is Branch. To satisfy the mutually exclusive property, it sends a Probe(toN) to L1-1. L1-1 reduces its permission to None and drops the data block and replies with ProbeAck(BtoN). L2 receives this message and sends GrantData(toT) to L1-2. Then, L1-1's AcquirePerm(BtoT)  has arrived but cannot make forward progress due to the pending GrantAck. Therefore,  L1-2 updates the state and responds to GrantData(toT) in the steps 8-9. After receiving the GrantAck, L2 is finally able to handle the AcquirePerm(BtoT) of L1-1.  However, at step 13, by our model, only the conditions L1-1's request being AcquirePerm(BtoT) and L1-1's state in L2's directory being Branch are satisfied,  L2 can do the next action to respond with the request(as the transition rule "Ln_receive_AcquirePerm_Tip_Branch_None" shows). However, it finds that L1-1 is in None state,  and has no rule that can be executed, so it enters a deadlock state. 

```
ruleset i : cacheId; b : branchType do 
rule "Ln_receive_AcquirePerm_Tip_Branch_None"
    !IsUndefined(Tree[i].chanA[b].opcode) &
    Tree[i].chanA[b].opcode = AcquirePerm &
    Tree[i].cache.state = Tip &
    forall k : branchType do
        (k != b -> Tree[i].directory[k] = None) &
        IsUndefined(Tree[i].slave_pending[k]) 
    endforall &
    Tree[i].directory[b] = Branch
==>
begin
    undefine Tree[i].chanA[b];
    Tree[i].chanD[b].opcode := Grant;
    Tree[i].chanD[b].para := toT;
    Tree[i].directory[b] := Tip;
    Tree[i].cache.state := Trunk;
    Tree[i].slave_pending[b] := pending_GrantAck;
endrule;
enruleset;
```

To deal with such a problem, we add a transition rule in the protocol: when receiving the AcquirePerm(BtoT) from a child, the parent checks the directory, if the state of the child is found to be None, the parent can respond, it needs to send not only the Tip permission but also the data block(as the transition rule "Ln_receive_AcquirePerm_Tip_None_None" shows). The deadlock is confirmed by the engineers who implement the protocol. After our modification, the model can run without any deadlok.

```
ruleset i : cacheId; b : branchType do 
rule "Ln_receive_AcquirePerm_Tip_None_None"
    !IsUndefined(Tree[i].chanA[b].opcode) &
    Tree[i].chanA[b].opcode = AcquirePerm &
    Tree[i].cache.state = Tip &
    forall k : branchType do
        (k != b -> Tree[i].directory[k] = None) &
        IsUndefined(Tree[i].slave_pending[k])
    endforall &
    Tree[i].directory[b] = None
==>
begin
    undefine Tree[i].chanA[b];
    Tree[i].chanD[b].opcode := GrantData;
    Tree[i].chanD[b].para := toT;
    Tree[i].chanD[b].data := Tree[i].cache.data;
    Tree[i].directory[b] := Tip;
    Tree[i].cache.state := Trunk;
    Tree[i].slave_pending[b] := pending_GrantAck;
endrule;
enruleset;
```

After modifying the models, we experimented with both TL-C-In and TL-C-NonIn protocol(The experimental environment is the same as in the section Reachability Analysis). The results are summarized in following table. The statistics includes 6 types of data: Protocol is the name of protocols we verified; #Rules refers to the number of transition rules of protocols; #Invs represents the number of properties that need to be verified by two protocols respectively; #Nodes is the number of cache nodes in the system: The full binary tree with two Levelss has 3 nodes, whereas the full binary tree with Three Levelss has 7 nodes; #States is the size of the state space; Time is the time (in seconds) spent in running the model. The state space of non-inclusive TL-C with 3-Levels and data control is too large to obtain the result in the case of running out of memory.

|     Protocol     | #Rules  | #Invs  | #Nodes  |   #States   |    Time   |
|:----------------:|:-------:|:------:|:-------:|:-----------:|:---------:|
|      TL-C-In     |    49   |    4   |    3    |    949      |    0.10   |
|                  |    49   |    4   |    7    | 83,663,100  |  1,953.37 |
|   TL-C-In_data   |    50   |    5   |    3    |    2,951    |    0.10   |
|                  |    50   |    5   |    7    | 548,368,860 | 18,022.25 |
|    TL-C-NonIn    |    64   |    3   |    3    |    2,475    |    0.10   |
|                  |    64   |    3   |    7    | 950,402,192 | 29,166.59 |
| TL-C-NonIn_data  |    65   |    4   |    3    |    39,663   |    0.47   |
|                  |    65   |    4   |    7    |      -      |     -     |

As the table shows, Inclusive mode is simpler than noninclusive in two ways: it requires fewer transition rules, and has less state space when the number of nodes is the same. Due to the memory limitations, the TL-C_NonIn_data protocol with 7 nodes terminated the model checking process before it searched the entire state space(and no errors are found). However, it can be proved that all the states of coherence tree are
actually reachable by reachability analysis, as shown in the Table in the Reachability Analysis.

<!--
2. The Inclusive mode should satisfy the *inclusive* property: for each cache block, if it is cached in a Treenode then it must also be cached in its parant node. Besides it should follow TileLink's guidelines. Therefore, the following requirements are put forward.
- A parent node in None state cannot have a child node that is not in None state. 
- A parent node in Trunk state can not have a child node in Branch state. 
- A parent node in Branch state can not have a Tip or Trunk child node. 
- The state of a Trunk node’s children is always Tip or Trunk. 
```
invariant "coherence_tree_1"
forall i : cacheId do
    Tree[i].cache.state = None ->
    forall b : branchType do
        Tree[i].directory[b] = None
    endforall
endforall;

invariant "coherence_tree_2"
forall i : cacheId do
    Tree[i].cache.state = Trunk ->
    forall b : branchType do
        Tree[i].directory[b] != Branch
    endforall
endforall;

invariant "coherence_tree_3"
forall i : cacheId do
    Tree[i].cache.state = Branch ->
    forall b : branchType do
        Tree[i].directory[b] != Tip
    endforall
endforall;

invariant "coherence_tree_4"
forall i : cacheId do
    Tree[i].cache.state = Trunk ->
    exists b : branchType do
        Tree[i].directory[b] = Tip
    end
endforall &
forall i : coreId do
    Tree[i].cache.state != Trunk
endforall;
```
3 In the Non-inclusive mode, any cache block in a node is not necessarily in its parent. Therefore, the invariants "coherence_tree_1" and "coherence_tree_4" in the inclusive mode is not accurate. In one case, by the definition of Non-inclusive, a cache block that is not kept in a node may still be in its children. In another case, the L3 cache node is in Trunk state, its child node in L2 is None, but the state of its grandchild node in L1 is Tip.

![Non-Inclusive states](./fig/NI_states.png)

Thus, for the noninclusive mode, suppose the priority of permissions from highest to lowest is: Tip>Trunk>Branch>None, we propose the following properties:
- For any subtree, the highest permission cannot be Trunk;
- For any parent node, if the highest permission of one of its subtrees is Tip, the highest permission of the other subtree and the parent node itself cannot be Branch.
We can use the directory in the parent node to represent the highest state in its subtree, and obtain the following equivalent invariants.
```
invariant "Axiom_2"
forall i : cacheId do
    Tree[i].cache.state = Trunk ->
    exists b :branchType do
        Tree[i].directory[b] = Tip
    end
endforall &
forall i : coreId do
    Tree[i].cache.state != Trunk 
endforall;

invariant "Axiom_3"
forall i : cacheId do
    forall b1 : branchType do
        forall b2 : branchType do
            b1 != b2 & Tree[i].directory[b1] = Tip -> Tree[i].cache.state != Branch & Tree[i].directory[b2] != Branch
        endforall
    endforall
endforall;
```
-->
