| state_no | IsReachable | Time(sec) | Trace file link              |
|----------|-------------|-----------|------------------------------|
| 1        | True        | 0.74s     | [trace_file](./state_1.log)  |
| 2        | True        | 0.76s     | [trace_file](./state_2.log)  |
| 3        | True        | 0.77s     | [trace_file](./state_3.log)  |
| 4        | True        | 0.75s     | [trace_file](./state_4.log)  |
| 5        | True        | 0.77s     | [trace_file](./state_5.log)  |
| 6        | True        | 0.76s     | [trace_file](./state_6.log)  |
| 7        | True        | 0.75s     | [trace_file](./state_7.log)  |
| 8        | True        | 0.77s     | [trace_file](./state_8.log)  |
| 9        | True        | 0.75s     | [trace_file](./state_9.log)  |
| 10       | True        | 0.77s     | [trace_file](./state_10.log) |
| 11       | True        | 0.75s     | [trace_file](./state_11.log) |
| 12       | True        | 0.76s     | [trace_file](./state_12.log) |
| 13       | True        | 0.76s     | [trace_file](./state_13.log) |
| 14       | True        | 0.76s     | [trace_file](./state_14.log) |
| 15       | True        | 0.76s     | [trace_file](./state_15.log) |
| 16       | True        | 0.75s     | [trace_file](./state_16.log) |
