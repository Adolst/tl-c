import re
import os
import argparse

murphi_path = '/home/lizm2022/cmurphi5.5.0'

def ProtocolConfig(filename, branch, allNodenum, coreNum):
    conf = []
    conf_area = False
    with open(filename, 'r') as f:
        for line in f.readlines():
            if "-- config start" in line:
                conf_area = True
                conf.append(line)
            elif "-- config end" in line:
                conf_area = False
                conf.append(line)
            elif conf_area:
                if "coreNum" in line:
                    conf.append(re.sub(r'\s*coreNum: \d*', '    coreNum: {}'.format(coreNum), line))
                elif "allNodeNum" in line:
                    conf.append(re.sub(r'\s*allNodeNum: \d+', '    allNodeNum: {}'.format(allNodenum), line))
                elif "allBranchNum" in line:
                    conf.append(re.sub(r'\s*allBranchNum: \d+', '    allBranchNum: {}'.format(branch), line))
            else:
                conf.append(line)

    with open(filename, 'w') as f:
        for line in conf:
            f.write('%s'%line) 

if __name__ == "__main__":
    
    arg_parser = argparse.ArgumentParser(description='Generate a tree with depth d and branches b')
    arg_parser.add_argument('-i', '--inclusion', help="Select the name of the protocol to generate", type = str, default = 'inclusive')
    arg_parser.add_argument('-c', '--datacontrol', help="Select the name of the protocol to generate", type = bool, default = False)
    arg_parser.add_argument('-d', '--depth', help="The depth", type = int, default = 2)
    arg_parser.add_argument('-b', '--branch', help="The number of branches", type = int, default = 2)
    args = arg_parser.parse_args()

    depth = args.depth
    branch = args.branch
    allNodenum = (branch**depth-1) // (branch-1)
    coreNum = branch**(depth-1)

    if args.inclusion == 'non-inclusive':
        mode = 'Non-Inclusive'
        protocol = 'TL-C_NonIn'
    else:
        mode = 'Inclusive'
        protocol = 'TL-C_In'

    if args.datacontrol:
        protocol += '_data'

    filename = '{}/{}'.format(mode, protocol)

    ProtocolConfig(filename+'.m', branch, allNodenum, coreNum)

    print("The file configuration is complete")

    os.system('mu {}.m'.format(filename))

    os.system('g++ -o {0}.o {0}.cpp -I {1}/include'.format(filename, murphi_path))